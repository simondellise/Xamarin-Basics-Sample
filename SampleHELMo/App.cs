﻿using System;

using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Forms.Mvvm;
using SampleHELMo.ViewModels;
using SampleHELMo.Views;
using Acr.UserDialogs;

namespace SampleHELMo
{
	public class App : Application
	{
		public App()
		{
			MainPage = new NavigationPage(new FriendsView());
		}

		#region Application states

		/* Pour les 3 méthodes ci-dessous, dans cet exemple nous affichons simplement une popup contenant un message 
		 * suivant l'état de l'application.
		 * 
		 * Ces popups nécessitent l'utilisation d'une librairie externe nommée "ACR User Dialogs for Xamarin and Windows"
		 * le package se retrouve via NuGet et se trouve dans ce projet dans le dossier Packages sous le nom "Acr.UserDialogs".
		 * 
		 * Idéalement, les messages devraient se trouver dans des constantes dans un fichier dédié à cela.
		 */

		protected override void OnStart()
		{
			UserDialogs.Instance.Alert("Démarrage de l'app", "Hello");
		}

		protected override void OnSleep()
		{
			// TODO To implement
		}

		protected override void OnResume()
		{
			UserDialogs.Instance.Alert("Réveil de l'app", "Hey", "Bonjour");
		}

		#endregion
	}
}

