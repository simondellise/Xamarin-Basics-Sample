﻿using System;
using SampleHELMo.ViewModels;

namespace SampleHELMo.Locators
{
	public static class ViewModelLocator
	{
		private static FriendsViewModel _friends = new FriendsViewModel();

		public static FriendsViewModel Friends
		{
			get
			{
				return _friends;
			}
			set
			{
				_friends = value;
			}
		}

		private static FriendDetailsViewModel _friendDetails = new FriendDetailsViewModel();

		public static FriendDetailsViewModel FriendDetails
		{
			get
			{
				return _friendDetails;
			}
			set
			{
				_friendDetails = value;
			}
		}
	}
}

