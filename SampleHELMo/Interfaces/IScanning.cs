﻿using System;
using System.Threading.Tasks;

namespace SampleHELMo
{
	public interface IScanning
	{
		Task<string> Scan();
	}
}

