﻿using XLabs.Forms.Mvvm;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using SampleHELMo.Locators;
using SampleHELMo.Views;

namespace SampleHELMo.ViewModels
{
	public class FriendsViewModel : ViewModel
	{
		#region Properties

		private ObservableCollection<FriendDetailsViewModel> _friends;

		public ObservableCollection<FriendDetailsViewModel> Friends
		{
			get { return _friends; }
			set { SetProperty(ref _friends, value); }
		}

		private FriendDetailsViewModel _selectedItem;

		public FriendDetailsViewModel SelectedItem
		{
			get { return _selectedItem; }
			set
			{
				SetProperty(ref _selectedItem, value);
				TappedCommandAction();
			}
		}


		private Command _tappedCommand;

		public Command TappedCommand
		{
			get { return _tappedCommand; }
			set { SetProperty(ref _tappedCommand, value); }
		}

		#endregion

		public FriendsViewModel()
		{
			CreateFriends();
		}

		private void TappedCommandAction()
		{
			if (SelectedItem != null)
			{
				ViewModelLocator.FriendDetails = SelectedItem;

				App.Current.MainPage.Navigation.PushAsync(new FriendDetailsView());
			}
		}

		/* Création des amis. En principe, ceux-ci devraient être récupérés d'un web service..
		 */
		private void CreateFriends()
		{
			Friends = new ObservableCollection<FriendDetailsViewModel>();
			Friends.Add(new FriendDetailsViewModel(new Friend{ id = 1, FirstName = "Ray", LastName = "Larson", Location = "Liège", PictureLocation = "https://randomuser.me/api/portraits/med/men/61.jpg" }));
			Friends.Add(new FriendDetailsViewModel(new Friend{ id = 2, FirstName = "Hannah", LastName = "Neal", Location = "Bruxelles", PictureLocation = "https://randomuser.me/api/portraits/med/women/90.jpg" }));
			Friends.Add(new FriendDetailsViewModel(new Friend{ id = 3, FirstName = "Noelle", LastName = "Ellis", Location = "Paris", PictureLocation = "https://randomuser.me/api/portraits/med/women/57.jpg" }));
		}

	}
}

