﻿using System;

using Xamarin.Forms;
using XLabs.Forms.Mvvm;
using SampleHELMo.Models;
using SampleHELMo.Locators;
using System.ComponentModel;
using Acr.UserDialogs;
using SampleHELMo.Models.DAO;

namespace SampleHELMo.ViewModels
{
	public class FriendDetailsViewModel : ViewModel
	{
		#region Properties

		private FriendDAO _entity = new FriendDAO();

		public FriendDAO Entity
		{
			get{ return _entity; }
			set
			{ 
				_entity = value;
				NotifyPropertyChanged(() => Entity);
			}
		}

		private Command _scanQRCodeCommand;

		public Command ScanQRCodeCommand
		{
			get
			{ 
				if (_scanQRCodeCommand == null)
					_scanQRCodeCommand = new Command(() => ScanQRCodeAction());
				
				return _scanQRCodeCommand;
			}
			set
			{ 
				_scanQRCodeCommand = value;
			}
		}

		#endregion

		public FriendDetailsViewModel()
		{
		}

		public FriendDetailsViewModel(Friend selectedItem)
		{
			Entity = new FriendDAO(selectedItem);
		}

		async void ScanQRCodeAction()
		{
			var result = await DependencyService.Get<IScanning>().Scan();
			UserDialogs.Instance.Alert(result, "Résultat du scan", "Fermer");
		}
	}
}


