﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using SampleHELMo.ViewModels;
using SampleHELMo.Locators;

namespace SampleHELMo.Views
{
	public partial class FriendsView : ContentPage
	{
		public FriendsView()
		{
			InitializeComponent();

			BindingContext = ViewModelLocator.Friends;
		}

		void TappedAction(object sender, EventArgs args)
		{
			this.listView.SelectedItem = null;	
		}
	}
}

