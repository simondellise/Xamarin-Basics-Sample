﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using SampleHELMo.Locators;

namespace SampleHELMo.Views
{
	public partial class FriendDetailsView : ContentPage
	{
		public FriendDetailsView()
		{
			InitializeComponent();

			BindingContext = ViewModelLocator.FriendDetails;
		}
	}
}

