﻿using System;
using XLabs.Forms.Mvvm;
using XLabs.Data;

namespace SampleHELMo.Models.DAO
{
	public class FriendDAO : ObservableObject
	{
		public FriendDAO()
		{
			
		}

		public FriendDAO(Friend baseModel)
			: this()
		{
			Entity = baseModel;
		}

		public Friend Entity
		{
			get;
			set;
		} = new Friend();


		public int Id
		{
			get { return Entity.id; }
			set
			{
				Entity.id = value;
				NotifyPropertyChanged(() => Id);
			}
		}

		public String FirstName
		{
			get { return Entity.FirstName; }
			set
			{
				Entity.FirstName = value;
				NotifyPropertyChanged(() => FirstName);
			}
		}

		public String LastName
		{
			get { return Entity.LastName; }
			set
			{
				Entity.LastName = value; 
				NotifyPropertyChanged(() => LastName);
			}
		}

		public String Location
		{
			get { return Entity.Location; }
			set
			{
				Entity.Location = value;
				NotifyPropertyChanged(() => Location);
			}
		}

		public String PictureLocation
		{
			get { return Entity.PictureLocation; }
			set
			{
				Entity.PictureLocation = value;
				NotifyPropertyChanged(() => PictureLocation);
			}
		}

	}
}

