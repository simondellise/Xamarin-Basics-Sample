﻿using System;

namespace SampleHELMo
{
	public class Friend
	{
		public int id { get; set; }

		public String FirstName { get; set; }

		public String LastName { get; set; }

		public String PictureLocation { get; set; }

		public String Location { get; set; }
	}
}

