﻿using Xamarin.Forms;
using System.Diagnostics;
using ZXing.Mobile;
using System.Threading.Tasks;
using System.Collections.Generic;
using ZXing;
using System;
using Android.Views;
using Android.Widget;
using SampleHELMo.Droid;

[assembly: Dependency(typeof(Scanning))]
namespace SampleHELMo.Droid
{
	public class Scanning : IScanning
	{
		#region IScanning implementation

		public async Task<string> Scan()
		{
			var scanner = new MobileBarcodeScanner();
			//scanner.CustomOverlay =

			var result = await scanner.Scan();
			if (result != null)
			{
				Debug.WriteLine("SCAN result: " + result.Text + " ; SCAN type: " + result.BarcodeFormat);
				return result.Text;
			}
			return "Return";
		}

		#endregion
	}
}

