﻿using System;
using ZXing.Mobile;
using ZXing;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using UIKit;
using SampleHELMo.iOS;

[assembly: Dependency(typeof(Scanning))]
namespace SampleHELMo.iOS
{
	public class Scanning : IScanning
	{
		#region IScanning implementation

		public async Task<string> Scan()
		{
			var scanner = new MobileBarcodeScanner();

			var result = await scanner.Scan();
			if (result != null)
			{
				Debug.WriteLine("SCAN result: " + result.Text + " ; SCAN type: " + result.BarcodeFormat);
				return result.Text;
			}
			return "Return";
		}

		#endregion
	}
}

